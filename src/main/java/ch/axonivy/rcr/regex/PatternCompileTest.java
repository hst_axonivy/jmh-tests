package ch.axonivy.rcr.regex;

import ch.axonivy.rcr.helper.OperationTypeImpl;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 2, jvmArgs = {"-Xms2G", "-Xmx2G"})
public class PatternCompileTest {

    @Param({"10", "100"})
    private int N;

    private static final String NAVIGATOR_CONTEXT_KEY_WITH_INDEX = "((\\w+\\:\\d+|\\w+|\\d+)\\.)+(\\w+\\:\\d+)";
    private static final Pattern pattern = Pattern.compile(NAVIGATOR_CONTEXT_KEY_WITH_INDEX);
    private static final Matcher matcher = Pattern.compile(NAVIGATOR_CONTEXT_KEY_WITH_INDEX).matcher("");

    private String keys[] = {"test", "5", "99"};

    private Map<String, OperationTypeImpl> DATA_FOR_TESTING;
    private Map<String, Boolean> CACHE = new HashMap<>();

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(PatternCompileTest.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }

    @Setup
    public void setup() {
        DATA_FOR_TESTING = createDataSimple();
    }

    private Map<String, OperationTypeImpl> createDataSimple() {
        Map<String, OperationTypeImpl> data = new HashMap<>();

        for (int i = 0; i < N; i++) {
            data.put("" + i, new OperationTypeImpl("Code_" + i,"Value_" + i, "" + i));
        }

        return data;
    }

    @Benchmark
    public void compilePattern(Blackhole blackhole) {

        for (int index = 0; index < DATA_FOR_TESTING.size(); index++) {

            blackhole.consume(Pattern.compile(NAVIGATOR_CONTEXT_KEY_WITH_INDEX).matcher(keys[index % 3]).matches());
        }
    }

    @Benchmark
    public void precompilePattern(Blackhole blackhole) {
        for (int index = 0; index < DATA_FOR_TESTING.size(); index++) {

            blackhole.consume(pattern.matcher(keys[index % 3]).matches());
        }
    }

    @Benchmark
    public void matcherReuse(Blackhole blackhole) {
        for (int index = 0; index < DATA_FOR_TESTING.size(); index++) {

            blackhole.consume(matcher.reset(keys[index % 3]).matches());
        }
    }

    @Benchmark
    public void cachedVersion(Blackhole blackhole) {
        String key;

        for (int index = 0; index < DATA_FOR_TESTING.size(); index++) {
            key = keys[index % 3];
            Boolean match = CACHE.get(key);

            if (match == null) {
                match = matcher.reset(key).matches();
                CACHE.put(key, match);
            }

            blackhole.consume(match);
        }
    }

    /*
    Results (higher Score means faster):

    Benchmark                             (N)   Mode  Cnt      Score     Error   Units
    PatternCompileTest.cachedVersion       10  thrpt    5  13424,331 ± 775,145  ops/ms
    PatternCompileTest.cachedVersion      100  thrpt    5   1311,624 ± 144,722  ops/ms
    PatternCompileTest.compilePattern      10  thrpt    5    114,859 ±   9,684  ops/ms
    PatternCompileTest.compilePattern     100  thrpt    5     11,249 ±   1,466  ops/ms
    PatternCompileTest.matcherReuse        10  thrpt    5    778,159 ±  39,819  ops/ms
    PatternCompileTest.matcherReuse       100  thrpt    5     77,331 ±  13,963  ops/ms
    PatternCompileTest.precompilePattern   10  thrpt    5    605,082 ±  82,992  ops/ms
    PatternCompileTest.precompilePattern  100  thrpt    5     60,173 ±   8,275  ops/ms
     */
}
