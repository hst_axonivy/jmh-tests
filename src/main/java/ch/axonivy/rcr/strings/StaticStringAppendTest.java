package ch.axonivy.rcr.strings;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 2, jvmArgs = {"-Xms2G", "-Xmx2G"})
public class StaticStringAppendTest {
    @Param({"10"})
    private int N;

    public static final String SPACE = " ";

    public static final String AFFORDABILITY_COCKPIT_VALUE_STYLE_CLASS = "affordability-cockpit add-ons-link curs-pointer";
    public static final String AFFORDABILITY_COCKPIT_VALUE_STYLE_CLASS_SPACE = AFFORDABILITY_COCKPIT_VALUE_STYLE_CLASS + SPACE;

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(StaticStringAppendTest.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }

    @Benchmark
    public void testStaticStringConcat(Blackhole blackhole) {
        String string;

        for (int i = 0; i < N; i++) {
            string = "".concat("" + i).concat(AFFORDABILITY_COCKPIT_VALUE_STYLE_CLASS).concat(SPACE).concat("" + i);

            blackhole.consume(string);
        }
    }

    @Benchmark
    public void testStaticStringAppend(Blackhole blackhole) {
        String string;

        for (int i = 0; i < N; i++) {
            string = i + AFFORDABILITY_COCKPIT_VALUE_STYLE_CLASS + SPACE + i;

            blackhole.consume(string);
        }
    }

    @Benchmark
    public void testStaticStringAlreadyAppended(Blackhole blackhole) {
        String string;

        for (int i = 0; i < N; i++) {
            string = i + AFFORDABILITY_COCKPIT_VALUE_STYLE_CLASS_SPACE + i;

            blackhole.consume(string);
        }
    }

    /*
    Results (higher Score means faster):

    Benchmark                                               (N)   Mode  Cnt     Score     Error   Units
    StaticStringAppendTest.testStaticStringAlreadyAppended   10  thrpt    5  4397,165 ± 746,629  ops/ms
    StaticStringAppendTest.testStaticStringAppend            10  thrpt    5  4063,782 ± 908,000  ops/ms
    StaticStringAppendTest.testStaticStringConcat            10  thrpt    5   844,303 ± 133,456  ops/ms
     */
}
