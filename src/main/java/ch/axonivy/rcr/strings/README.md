# StaticStringAppendTest

Results (higher Score means faster):
```
Benchmark                                               (N)   Mode  Cnt     Score     Error   Units
StaticStringAppendTest.testStaticStringAlreadyAppended   10  thrpt    5  4397,165 ± 746,629  ops/ms
StaticStringAppendTest.testStaticStringAppend            10  thrpt    5  4063,782 ± 908,000  ops/ms
StaticStringAppendTest.testStaticStringConcat            10  thrpt    5   844,303 ± 133,456  ops/ms
```

https://jira.axonivy.com/confluence/display/RCR/JMH...+Do+not+forget+to+flush+things+into+the+Blackhole

# StringAppendVSConcatTest

Results (higher Score means faster):
```
Benchmark                                                         (N)   Mode  Cnt     Score     Error   Units
StringAppendVSConcatTest.testHelloWorldConcat                      10  thrpt    5  2151,971 ± 213,309  ops/ms
StringAppendVSConcatTest.testHelloWorldPlus                        10  thrpt    5  4459,290 ± 490,473  ops/ms
StringAppendVSConcatTest.testHelloWorldStringBuilderOneAppend      10  thrpt    5  3479,616 ± 356,015  ops/ms
StringAppendVSConcatTest.testHelloWorldStringBuilderThreeAppends   10  thrpt    5  3385,469 ± 558,967  ops/ms
```