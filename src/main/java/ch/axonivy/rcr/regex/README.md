# PatternCompileTest

Results (higher Score means faster):
```
Benchmark                             (N)   Mode  Cnt      Score     Error   Units
PatternCompileTest.cachedVersion       10  thrpt    5  13424,331 ± 775,145  ops/ms
PatternCompileTest.cachedVersion      100  thrpt    5   1311,624 ± 144,722  ops/ms
PatternCompileTest.compilePattern      10  thrpt    5    114,859 ±   9,684  ops/ms
PatternCompileTest.compilePattern     100  thrpt    5     11,249 ±   1,466  ops/ms
PatternCompileTest.matcherReuse        10  thrpt    5    778,159 ±  39,819  ops/ms
PatternCompileTest.matcherReuse       100  thrpt    5     77,331 ±  13,963  ops/ms
PatternCompileTest.precompilePattern   10  thrpt    5    605,082 ±  82,992  ops/ms
PatternCompileTest.precompilePattern  100  thrpt    5     60,173 ±   8,275  ops/ms
```