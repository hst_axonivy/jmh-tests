package ch.axonivy.rcr.strings;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 2, jvmArgs = {"-Xms2G", "-Xmx2G"})
public class StringAppendVSConcatTest {
    @Param({"10"})
    private int N;

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(StringAppendVSConcatTest.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();

        // Check if results are the same:
//        String helloWorldString = "";
//        int N = 100;
//
//        for (int i = 0; i < N; i++) {
//            helloWorldString += "HelloWorld{" + i + "}";
//        }
//
//        String helloWorldString2 = "";
//
//        for (int i = 0; i < N; i++) {
//            helloWorldString2 = helloWorldString2.concat("HelloWorld{" + i + "}");
//        }
//
//        StringBuilder helloWorldStringStringBuilder = new StringBuilder();
//
//        for (int i = 0; i < N; i++) {
//            helloWorldStringStringBuilder.append("HelloWorld{" + i + "}");
//        }
//
//        StringBuilder helloWorldStringStringBuilder2 = new StringBuilder();
//
//        for (int i = 0; i < N; i++) {
//            helloWorldStringStringBuilder2.append("HelloWorld{");
//            helloWorldStringStringBuilder2.append(i);
//            helloWorldStringStringBuilder2.append("}");
//        }
//
//        System.out.println("helloWorldString              :" + helloWorldString);
//        System.out.println("helloWorldString2             :" + helloWorldString2);
//        System.out.println("helloWorldStringStringBuilder :" + helloWorldStringStringBuilder.toString());
//        System.out.println("helloWorldStringStringBuilder2:" + helloWorldStringStringBuilder2.toString());
    }

    @Benchmark
    public void testHelloWorldPlus(Blackhole blackhole) {
        String helloWorldString = "";

        for (int i = 0; i < N; i++) {
            helloWorldString += "HelloWorld{" + i + "}";
        }

        blackhole.consume(helloWorldString);
    }

    @Benchmark
    public void testHelloWorldConcat(Blackhole blackhole) {
        String helloWorldString = "";

        for (int i = 0; i < N; i++) {
            helloWorldString = helloWorldString.concat("HelloWorld{" + i + "}");
        }

        blackhole.consume(helloWorldString);
    }

    @Benchmark
    public void testHelloWorldStringBuilderOneAppend(Blackhole blackhole) {
        StringBuilder helloWorldStringStringBuilder = new StringBuilder();

        for (int i = 0; i < N; i++) {
            helloWorldStringStringBuilder.append("HelloWorld{" + i + "}");
        }

        blackhole.consume(helloWorldStringStringBuilder.toString());
    }

    @Benchmark
    public void testHelloWorldStringBuilderThreeAppends(Blackhole blackhole) {
        StringBuilder helloWorldStringStringBuilder = new StringBuilder();

        for (int i = 0; i < N; i++) {
            helloWorldStringStringBuilder.append("HelloWorld{");
            helloWorldStringStringBuilder.append(i);
            helloWorldStringStringBuilder.append("}");
        }

        blackhole.consume(helloWorldStringStringBuilder.toString());
    }

    /*
    Results (higher Score means faster):

    Benchmark                                                         (N)   Mode  Cnt     Score     Error   Units
    StringAppendVSConcatTest.testHelloWorldConcat                      10  thrpt    5  2151,971 ± 213,309  ops/ms
    StringAppendVSConcatTest.testHelloWorldPlus                        10  thrpt    5  4459,290 ± 490,473  ops/ms
    StringAppendVSConcatTest.testHelloWorldStringBuilderOneAppend      10  thrpt    5  3479,616 ± 356,015  ops/ms
    StringAppendVSConcatTest.testHelloWorldStringBuilderThreeAppends   10  thrpt    5  3385,469 ± 558,967  ops/ms
     */
}
