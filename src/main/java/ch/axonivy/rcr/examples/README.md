# FibonacciTest

We test how fast an empty benchmark runs (testEmpty), vs. a performance test where we forgot to use the blackhole 
(testFibonacciAnswerWasted) vs. the correct usage of the blackhole (testFibonacciAnswerConsume).

Results (higher Score means faster):
```
Benchmark                                     (N)   Mode  Cnt        Score        Error   Units
FibonacciTest.testEmpty                   1000000  thrpt    5  3037127,617 ± 778534,191  ops/ms
FibonacciTest.testFibonacciAnswerConsume  1000000  thrpt    5        0,126 ±      0,025  ops/ms
FibonacciTest.testFibonacciAnswerWasted   1000000  thrpt    5  1104959,265 ± 932662,170  ops/ms
```

https://jira.axonivy.com/confluence/display/RCR/JMH...+Do+not+forget+to+flush+things+into+the+Blackhole