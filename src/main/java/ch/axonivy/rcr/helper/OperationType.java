package ch.axonivy.rcr.helper;

public interface OperationType {
    String getCode();
    String getValue();
    String getCmsUri();
}
