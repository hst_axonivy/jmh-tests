package ch.axonivy.rcr.examples;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 2, jvmArgs = {"-Xms2G", "-Xmx2G"})
public class FibonacciTest {
    @Param({"1000000"})
    private int N;

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(FibonacciTest.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }

    private static int NUMBER = 25;

    private static int calcFibonacci(int n) {
        int result = 1;
        int prev = -1;
        int sum;

        for (int i = 0; i <= n; i++) {
            sum = prev + result;
            prev = result;
            result = sum;
        }

        return result;
    }

    @Benchmark
    public void testEmpty(Blackhole blackhole) {
    }

    @Benchmark
    public void testFibonacciAnswerWasted(Blackhole blackhole) {
        for (int i = 0; i < N; i++) {
            calcFibonacci(NUMBER);
        }
    }

    @Benchmark
    public void testFibonacciAnswerConsume(Blackhole blackhole) {
        for (int i = 0; i < N; i++) {
            blackhole.consume(calcFibonacci(NUMBER));
        }
    }

    /*
    Results (higher Score means faster):

    Benchmark                                     (N)   Mode  Cnt        Score        Error   Units
    FibonacciTest.testEmpty                   1000000  thrpt    5  3037127,617 ± 778534,191  ops/ms
    FibonacciTest.testFibonacciAnswerConsume  1000000  thrpt    5        0,126 ±      0,025  ops/ms
    FibonacciTest.testFibonacciAnswerWasted   1000000  thrpt    5  1104959,265 ± 932662,170  ops/ms
     */
}