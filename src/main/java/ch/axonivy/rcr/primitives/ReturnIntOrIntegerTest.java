package ch.axonivy.rcr.primitives;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.*;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 2, jvmArgs = {"-Xms2G", "-Xmx2G"})
public class ReturnIntOrIntegerTest {
    @Param({"1000"})
    private int N;

    private List<String> DATA_FOR_TESTING;

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(ReturnIntOrIntegerTest.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }

    @Setup
    public void setup() {
        DATA_FOR_TESTING = createData();
    }

    private List<String> createData() {
        List<String> data = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            data.add("Number : " + i);
        }

        return data;
    }

    private Integer objectSize = new Integer(1000);

    private int primitiveSize = 1000;

    private Integer getIntegerSize() {
        return this.objectSize;
    }

    private int getIntSize() {
        return this.primitiveSize;
    }

    @Benchmark
    public void loopIntPrimitive(Blackhole blackhole) {
        String foo;

        for (int i = 0; i < getIntSize(); i++) {
            foo = DATA_FOR_TESTING.get(i);
            blackhole.consume(foo);
        }
    }

    @Benchmark
    public void loopIntegerObject(Blackhole blackhole) {
        String foo;

        for (int i = 0; i < getIntegerSize(); i++) {
            foo = DATA_FOR_TESTING.get(i);
            blackhole.consume(foo);
        }
    }

    /*
    Results (higher Score means faster):

    Benchmark                                  (N)   Mode  Cnt    Score    Error   Units
    ReturnIntOrIntegerTest.loopIntPrimitive   1000  thrpt    5  247,170 ± 31,747  ops/ms
    ReturnIntOrIntegerTest.loopIntegerObject  1000  thrpt    5  231,407 ± 33,375  ops/ms
     */
}

