# ReturnIntOrIntegerTest

Results (higher Score means faster):
```
Benchmark                                  (N)   Mode  Cnt    Score    Error   Units
ReturnIntOrIntegerTest.loopIntPrimitive   1000  thrpt    5  247,170 ± 31,747  ops/ms
ReturnIntOrIntegerTest.loopIntegerObject  1000  thrpt    5  231,407 ± 33,375  ops/ms
```