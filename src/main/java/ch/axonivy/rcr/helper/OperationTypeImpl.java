package ch.axonivy.rcr.helper;

public class OperationTypeImpl implements OperationType {
    private String code;
    private String value;
    private String cmsUri;

    public OperationTypeImpl(String code, String value, String cmsUri) {
        this.code = code;
        this.value = value;
        this.cmsUri = cmsUri;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public String getCmsUri() {
        return this.cmsUri;
    }
}
